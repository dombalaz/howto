#!/bin/zsh

# This script creates swap file
# Parameter:
#   * path to swap file
#   * size in GiB

if [ "$EUID" -ne 0 ]
then
    echo "Please run as root"
    exit
fi

# Check parameters
if [ $# -ne 2 ]
then
    echo "Usage: zsh create_swap.zsh <path-to-swap> <size-in-GiB>"
    exit
fi

set -e

# Create swap file
mkdir -p "$(dirname $1)" && touch $1

dd status=none if=/dev/zero of=$1 bs=1024 count="$2"M

echo "\n# Swap file" >> /etc/fstab
echo "$1 swap swap sw 0 0" >> /etc/fstab

echo "Swap file created"
echo "Reboot system or activate swap:"
echo "swapon $1"
